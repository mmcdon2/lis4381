# LIS4381 - Mobile Web Application Development

## Michael McDonald

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Git
    - Create Bitbucket repo
    - Install Visual Studio Code
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Install AMPPS
    - Install Java
    - Complete Getting Started with Java in Visual Studio Code
    - Install Android Studio
    - Provide screenshots of installations
    - Provide Git command descriptions
    - Provide Bitbucket repo links

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Complete Healthy Recipe tutorial
    - Complete Create a mobile recipe app using Android Studio
    - Provide screenshots of interfaces
    - Change background and text color of both interfaces
    - Provide screenshots of skillsets
    - Provide Bitbucket repo links

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Install MySQL Workbench
    - Complete Database Requirements tutorial
    - Complete Mobile App Requirements tutorial
    - Provide screenshot of ERD
    - Provide screenshot of interfaces
    - Provide screenshots of 10 records for each table
    - Provide links to a3 files

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a business card app using Android Studio
    - Provide screenshots of interfaces
    - Change background of both interfaces
    - Add border to app image
    - Add text shadow to app button
    - Provide screenshots of skillsets
    - Provide Bitbucket repo link

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Complete Petstore validation screen
    - Complete Assignment 4 tutorial
    - Provide screenshot of Portal main page
    - Provide screenshot of validation interfaces
    - Provide screenshots of skillsets
    - Provide Bitbucket repo link

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete Pet Stores validation screen
    - Complete Basic Server-side validation screen
    - Complete Assignment 5 tutorial
    - Provide screenshots of Pet Stores valid and invalid input
    - Provide screenshots of Skillsets
    - Provide Bitbucket repo link

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Complete Pet Stores data table code modifications
    - Complete Project 5 tutorials
    - Provide screenshots of index.php and edit_petstore.php
    - Provide screenshots of failed and passed validations
    - Provide screenshot of RSS Feed
    - Provide Bitbucket repo link 
