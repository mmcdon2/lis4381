# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Assignment 4 Requirements:

*Six Parts*

1. Move local repos directory to www directory
2. Clone assignment starter student files
3. Modify index.php file for petstore entity
4. Create a favicon for each assignment's main directory
5. Complete skillsets 10 through 12
6. Helper_video links: a) this assignment


#### README.md file should include the following items:

* Assignment 4 requirements
* Screenshot of portal main page
* Screenshot of failed validation
* Screenshot of passed validation
* Screenshot of skillsets 10, 11, and 12
* Link to local lis4381 web app


#### Assignment Screenshots:

*Screenshot of portal main page*:

![LIS4381 Portal (Main page)](img4/LIS4381_Portal.png)

*Screenshot of failed validation*:

![Failed validation](img4/failed_validation.png)

*Screenshot of passed validation*:

![Passed validation](img4/passed_validation.png)

*Screenshot of Skillset 10 - ArrayList*:

![ArrayList](img4/ArrayList.png)

*Screenshot of Skillset 11 - AlphaNumericSpecial*:

![AlphaNumericSpecial](img4/Alpha_Numeric_Special.png)

*Screenshot of Skillset 12 - TemperatureConversion*:

![TemperatureConversion](img4/Temperature_Conversion.png)


#### Tutorial Links:

* Tutorial - Helper_videos:*

[A4 Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A4a.mp4)

[A4 Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A4b.mp4)
