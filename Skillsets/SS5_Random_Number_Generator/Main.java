import java.util.Arrays; //Note: only needed for Arrays.toString() method demo below

public class RandomNumberGenerator {
    public static void main(String args[]) {
        // Call static void method (i.e., no object, non-value returning)
        RandomNumberGeneratorMethods.getRequirements(); // *Only* way to call void method: in stand-alone statement!

        int[] userArray = RandomNumberGeneratorMethods.createArray(); // Java style array

        RandomNumberGeneratorMethods.generatePseudoRandomNumbers(userArray); // pass array

    }
}