import java.util.Scanner;

public class Methods {
        // Create Method without returning any value (without object)
        public static void getRequirements() {
                // display operational messages
                System.out.println("Program prompts user for first name and age, then prints results.");
                System.out.println("Create four methods from the following requirements.");
                System.out.println("1) getRequirements(): Void method displays program requirements.");
                System.out.println(
                                "2) getUserInput(): Void method prompts for user input, \n\then calls two methods: myVoidMethod() and myValueReturningMethod().");
                System.out.println("3) myVoidMethod():\n" +
                                "\ta. Accepts two arguements: String and int. \n" +
                                "\tb. Prints user's first name and age.");
                System.out.println("4) myValueReturningMethod():\n" +
                                "\ta. Accepts two arguements: String and int. \n" +
                                "\tb. Returns String containing first name and age.");

                System.out.println(); // print blank line
        }

        public static void getUserInput() {
                // initialize variables, create Scanner object, capture user input
                String firstName = "";
                int userAge = 0;
                String myStr = "";
                try (Scanner sc = new Scanner(System.in)) {
                        {
                                // input
                                System.out.println("Enter first name: ");
                                firstName = sc.next();

                                System.out.println("Enter age: ");
                                userAge = sc.nextInt();
                        }
                }
                System.out.println(); // print blankl line

                // Note: done for simplicity--method/function calls *should* go back to calling
                // environment
                // call void method
                System.out.println("void method call: ");
                myVoidMethod(firstName, userAge);

                // call value-returning method
                System.out.println("value-returning method call: ");
                myStr = myValueReturningMethod(firstName, userAge);
                // myValueReturningMethod(firstName, userAge); //Works, but discarded!
                System.out.println(myStr);
        }

        // Note: both methods use *same* named parameters--which are *local* variables!
        // Also, both methods are static--that is, can be used w/o instantiating objects
        public static void myVoidMethod(String first, int age) {
                System.out.println(first + " is " + age);
                return; // Ok, with or without return statement

        }

        public static String myValueReturningMethod(String first, int age) {
                // note: implicit string conversion of age (int)
                return first + " is " + age;
                // return age; //Error! Attempts to return different data type (int) than
                // defined (String)!

        }
}