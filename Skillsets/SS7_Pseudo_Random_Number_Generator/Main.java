//Javadoc: documentation generator for generating
//API documentation in HTML format from Java source code.
//https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html

public class PseudoRandomNumberGenerator {
    public static void main(String args[]) {
        // call static void methods (i.e. no object, non-value returning)
        PseudoRandomNumberGeneratorMethods.getRequirements();

        // Java style String[] myArray
        // C++ style String myArray[]
        // call createArray() method in Methods class
        // returns initialized array, array size determined by user
        int[] userArray = PseudoRandomNumberGeneratorMethods.createArray(); // Java style array

        // call generatePseudoRandomNumber() method, passing returned array above
        // prints psuedo-randomly generated numbers, determined by number user inputs
        PseudoRandomNumberGeneratorMethods.generatePseudoRandomNumbers(userArray); // pass array
    }

}
