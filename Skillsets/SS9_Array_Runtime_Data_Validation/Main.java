public class ArrayRuntime {
    public static void main(String args[]) {
        // call static void methods (i.e., no object, nonvalue-returning)
        ArrayRuntimeMethods.getRequirements();

        // Java style String[] myArray
        // C++ style String myArray[]
        // returns initialized array, array size determined by user
        int arraySize = 0;
        arraySize = ArrayRuntimeMethods.validateArraySize(); // Java style array

        /*
         * //printing array values for testing purposes only!
         * for (int i = 0; i < userArray.length; i++)
         * System.out.println(userArray[i] + ",");
         */

        // call method, passing returned array above
        // after processing, method calls another method
        ArrayRuntimeMethods.calculateNumbers(arraySize);
    }
}