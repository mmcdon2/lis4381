import java.util.Scanner;

public class ArrayRuntimeMethods {
    // create global scanner object, used in more than one method
    // Note: using "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    // nonvalue-returning method (static requires no object)
    public static void getRequirements() {
        // display operational messages
        System.out.println("Developer: Michael McDonald");
        System.out.println("Program creates array size at runtime.");
        System.out.println("Program displays array size.");
        System.out.println("Program rounds sum and average of numbers to two decimal places.");
        System.out.println("Numbers *must* be float data type, not double");

    }

    // value-returning method (static requires no object)
    public static int validateArraySize() {
        // declare variables and create Scanner object
        // Scanner sc = new Scanner(System.in);
        int arraySize = 0;

        // prompt user for array size
        System.out.println("Please enter array size: ");
        while (!sc.hasNextInt()) {
            System.out.println("Not valid integer!\n");
            sc.next(); // Important! If omitted, will go into infinite loop on invalid input!
            System.out.println("Please try again. Enter array size: ");
        }
        arraySize = sc.nextInt();
        System.out.println(); // print blank line

        // return array size to callingenvironment
        return arraySize;
    }

    // nonvalue-returning method (static requires no object)
    public static void calculateNumbers(int arraySize) {
        float sum = 0.0f;
        float average = 0.0f;

        // indicate number of values require, based upon user input
        System.out.println("Please enter " + arraySize + " numbers.\n");

        // create array for storing user input, based upon user-entered array size
        float numsArray[] = new float[arraySize];

        // validate data entry
        for (int i = 0; i < arraySize; i++) {
            System.out.println("Enter num " + (i + 1) + ":");

            while (!sc.hasNextFloat()) {
                System.out.println("Not valid number!\n");
                sc.next(); // Important! If omitted, will go into infinite loop on invalid input!
                System.out.println("Please try again. Enter num " + (i + 1) + ":");
            }
            numsArray[i] = sc.nextFloat(); // capture validated user input
            sum = sum + numsArray[i]; // process data entry
        }
        average = sum / arraySize;

        // print numbers entered
        System.out.println("\nNumbers entered: ");
        for (int i = 0; i < numsArray.length; i++)
            System.out.println(numsArray[i] + "");

        // call method to print and format numbers
        printNumbers(sum, average);
    }

    // nonvalue-returning method (static requires no object)
    public static void printNumbers(float sum, float average) {
        System.out.println(); // print blank line
        System.out.println("\nSum: " + String.format("%.2f", sum));
        System.out.println("Average: " + String.format("%.2f", average));

    }
}
