<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);  //turn off to show "Error in pattern"
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//$comments = trim($_POST['comments']);


// or, for nicer display in browser...
//echo "<pre>";
//print_r($_POST);
//echo "</pre>";
//exit(); //stop processing, otherwise, errors below
//After testing, comment out above lines.

//use for initial test of form inputs--also, demo no htmlspecialchars()
//exit(print_r($_POST));

/*
    Best practice: sanitize input - prepared statements, and escape output - htmlspecialchars().

    htmlspecialchars() helps protect against cross-site scripting (XSS).
    XSS enables attackers to inject client-side script into Web pages viewed by other users

    Note: call htmlspecialchars() when echoing data into HTML.
    However, don't store escaped HTML in your database.
    The database should store actual data, not its HTML representation.
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Michael McDonald">
	<link rel="icon" href="img/favicon.ico">

    <title>Write/Read File</title>
        
    <?php include_once("../css/include_css.php"); ?>

</head>

<body>

    <?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("global/header.php"); ?>
    </div>
    <p class="text-justify">
    <?php
    //create/open file for appending write/read
    // Note: w+ = Read/Write. Opens and truncates file, that is, removes existing data (or creates new file, if not exists). Starts at beginning of file.
        $myfile = fopen("file.txt", "w+") or exit("Unable to open file!");
        $txt = $_POST['comment'];
        fwrite($myfile, $txt); //write to file
        fclose($myfile); //close file

        //Note: r+ = Read/Write. Starts at beginning of file.
            $myfile = fopen("file.txt", "r+") or exit("Unable to open file!");
            //output one line until end-of-file
            while(!feof($myfile)) {
                echo fgets($myfile) . "<br />";
            }
            //echo fread($myfile, filesize("file.txt")); //read from file
            fclose($myfile); //close file
 
        ?>
        </p>

        <?php include_once "global/footer.php"; ?>

    </div> <!--starter-template -->
</div> <!--end container -->

<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>

    </body>
</html>
    