# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Assignment 1 Requirements:

*Four Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)


#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* Git commands w/short descriptions


#### Git commands w/short descriptions:

1. git init - Create a new local directory 
2. git status - List the status of the files you've changed and those you still need to add or commit
3. git add - Add a specific file to staging or after a new file is created
4. git commit - Commit changes locally
5. git push - Push changes to your remote repository
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git clone - Copy a remote repository to your local system

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/a1_ampps_installation.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/a1_JDK_javaHello.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/a1_AndroidStudio_MyFirstApp.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mmcdon2/bitbucketstationlocations/ "Bitbucket Station Locations")