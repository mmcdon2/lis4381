# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Assignment 2 Requirements:

*Five Parts*

1. Create a mobile recipe app using Android Studio
2. Questions
3. Helper_video link: a) this assignment
4. Must change the background and text colors in both activities, or use background image
5. Complete skillsets 1 through 3


#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of skillsets 1, 2, and 3


#### Assignment Screenshots:

*Screenshot of running application*:

![Healthy Recipe first interface](img2/HealthyRecipe_Screen1.png)

*Screenshot of running application*:

![Healthy Recipe second interface](img2/HealthyRecipe_Screen2.png)

*Screenshot of Skillset 1 - Even Or Odd*:

![EvenOrOdd](img2/EvenOrOdd.png)

*Screenshot of Skillset 2 - Largest Number*:

![LargestNumber](img2/LargestNumber.png)

*Screenshot of Skillset 3 - Arrays and Loops*:

![ArraysAndLoops](img2/ArraysAndLoops.png)




#### Tutorial Link:

* Tutorial - Helper_video:*
[A2 Healthy Recipe Tutorial Link](http://www.qcitr.com/vids/LIS4381A2.mp4/ "Helper_Video")
