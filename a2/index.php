<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Michael McDonald II">
    <link rel="icon" href="img2/favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 1. Create a mobile recipe app using Android Studio
2. Questions
3. Helper_video link: a) this assignment
4. Must change the background and text colors in both activities, or use background image
5. Complete skillsets 1 through 3 
				</p>

				<h4>Recipe Interface 1</h4>
				<img src="img2/HealthyRecipe_Screen1.png" class="img-responsive center-block" alt="Recipe Interface 1">

				<h4>Recipe Interface 2</h4>
				<img src="img2/HealthyRecipe_Screen2.png" class="img-responsive center-block" alt="Recipe Interface 2">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
