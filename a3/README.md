# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Assignment 3 Requirements:

*Five Parts*

1. Create a three table pet store database using MySQL Workbench
2. Create a mobile concert ticket buying app using Android Studio
3. Must change the background and text colors in both activities, or use background image
4. Complete skillsets 4 through 6
5. Helper_video links: a) this assignment


#### README.md file should include the following items:

* Screenshot of running application's opening user interface
* Screenshot of running application's processing user interface
* Screenshot of database tables
* Screenshot of skillsets 4, 5, and 6


#### Assignment Screenshots:

*Screenshot of running application*:

![Pet store table](img3/petstore.png)

*Screenshot of running application*:

![Customer table](img3/customer.png)

*Screenshot of running application*:

![Pet table](img3/pet.png)

*Screenshot of running application*:

![Concert first interface](img3/concert_screen1.png)

*Screenshot of running application*:

![Concert second interface](img3/concert_screen2.png)

*Screenshot of Skillset 4 - Decision Structures*:

![EvenOrOdd](img3/DecisionStructures.png)

*Screenshot of Skillset 5 - Random Number Generator*:

![LargestNumber](img3/RandomNumberGenerator.png)

*Screenshot of Skillset 6 - Methods*:

![ArraysAndLoops](img3/Methods.png)


#### Links to SQL files:

[a3.sql file](docs/a3.sql)

[a3.mwb file](docs/a3.mwb)


#### Tutorial Links:

* Tutorial - Helper_videos:*

[A3 Database Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A3_Database_Requirements.mp4)

[A3 Mobile App Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A3.mp4)
