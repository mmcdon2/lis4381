<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Michael McDonald II">
    <link rel="icon" href="img3/favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
1. Create a three table pet store database using MySQL Workbench
2. Create a mobile concert ticket buying app using Android Studio
3. Must change the background and text colors in both activities, or use background image
4. Complete skillsets 4 through 6
5. Helper_video links: a) this assignment 
				</p>

				<h4>Petstore table</h4>
				<img src="img3/petstore.png" class="img-responsive center-block" alt="Petstore">

				<h4>Customer table</h4>
				<img src="img3/customer.png" class="img-responsive center-block" alt="Customer">

				<h4>Pet table</h4>
				<img src="img3/pet.png" class="img-responsive center-block" alt="Pet">

				<h4>ERD</h4>
				<img src="img3/ERD.png" class="img-responsive center-block" alt="ERD">

				<h4>Event Interface 1</h4>
				<img src="img3/concert_screen1.png" class="img-responsive center-block" alt="Event Interface 1">

				<h4>Event Interface 2</h4>
				<img src="img3/concert_screen2.png" class="img-responsive center-block" alt="Event Interface 2">
				
				
				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
