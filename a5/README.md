# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Assignment 5 Requirements:

*Five Parts*

1. Clone A4 files
2. Copy A4 index.php file and rename it add_petstore.php
3. Open add_petstore.php and modify meta tags and code
4. Complete skillsets 13 through 15
5. Helper_video links: a) this assignment


#### README.md file should include the following items:

* Assignment 5 requirements
* Screenshot of add_petstore.php valid and invalid output
* Screenshot of add_Petstore_process.php valid and invalid output
* Screenshot of passed and failed validation
* Screenshot of skillsets 13, 14, and 15
* Link to local lis4381 web app


#### Assignment Screenshots:

*Screenshot of index.php*:

![Index.php](img/index.php.png)

*Screenshot of invalid input*:

![Invalid input](img/add_petstore.php_invalid.png)

*Screenshot of valid input*:

![Valid input](img/add_petstore.php_valid.png)

*Screenshot of failed validation*:

![Failed validation](img/add_petstore_process.php_failed.png)

*Screenshot of passed validation*:

![Passed validation](img/add_petstore_process.php_passed.png)

*Screenshot of Skillset 13 - SphereVolumeCalculator*:

![Sphere Volume Calculator](img/Sphere_Volume_Calculator.png)

*Screenshot 1 of Skillset 14 - PHP:Simple Calculator (index.php)*:

![PHP:Simple Calculator](img/Simple_Calculator_index.php.png)

*Screenshot 2 of Skillset 14 - PHP:Simple Calculator (index.php)*:

![PHP:Simple Calculator](img/Simple_Calculator_index.php2.png)

*Screenshot 3 of Skillset 14 - PHP:Simple Calculator (process_function)*:

![PHP:Simple Calculator](img/Simple_Calculator_process_function.php.png)

*Screenshot 4 of Skillset 14 - PHP:Simple Calculator (process_function)*:

![PHP:Simple Calculator](img/Simple_Calculator_process_function.php2.png)

*Screenshot 1 of Skillset 15 - PHP:Write/ReadFile (index.php)*:

![PHP:Write/Read File](img/Write_Read_File_index.php.png)

*Screenshot 2 of Skillset 15 - PHP:Write/ReadFile (process.php)*:

![PHP:Write/Read File](img/Write_Read_File_process.php.png)

#### Tutorial Links:

* Tutorial - Helper_videos:*

[A5 Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A5a.mp4)

[A5 Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A5b.mp4)

[A5 Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A5c.mp4)

