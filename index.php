<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My interactive portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Michael McDonald II">
		<link rel="icon" href="img/favicon.ico">
		<title>Michael's Interactive Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #FFFFFF;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="1000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/Matrix_2.jpg) no-repeat left center; background-size: cover;">
							<h2>Michael McDonald II</h2>
								<div class="container">
									<div class="carousel-caption">
										<h3>Web Developer</h3>
										<p>Let's see how far the rabbit hole goes...</p>
                        </div>
                      </div>
                    </div>
              
							<div class="item" style="background: url(img/Mario_mushroom.jpg) no-repeat left center; background-size: cover;">
							
								<h2>Michael McDonald II</h2>								
								<div class="carousel-caption">
									<h3>Commercial Artist</h3>
									<p>Don't be afraid to fail, be afraid to not try...</p>
																		
								</div>
							</div>

							<div class="item" style="background: url(img/Red_Matrix_1.jpg) no-repeat left center; background-size: cover;">
								<h2>Michael McDonald II</h2>							
								<div class="carousel-caption">
									<h3>Computer Programmer</h3>
									<p>Everything's impossible until somebody does it....</p>
																		
								</div>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
