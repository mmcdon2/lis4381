# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Project 1 Requirements:

*Five Parts*

1. Create a mobile business card app using Android Studio
2. Must change the background in both activities, add border to app image, and add text shadow to app button
3. Complete skillsets 7 through 9
4. Provide screenshots of both interfaces
5. Complete Project 1 Questions


#### README.md file should include the following items:

* Screenshot of running application's opening user interface
* Screenshot of running application's processing user interface
* Screenshot of skillsets 7, 8, and 9


#### Assignment Screenshots:

| Screenshot of running opening user interface | Screenshot of running user input |
| ---------- | ---------- |
| ![Business Card first interface](imgP/business_card1.png) | ![Business Card second interface](imgP/business_card2.png) |

*Screenshot of Skillset 7 - Random Number Generator Data Validation*:

![PseudoRandomNumberGenerator](imgP/PseudoRandomNumberGenerator.png)

*Screenshot of Skillset 8 - Largest Three Numbers*:

![LargestThreeNumbers](imgP/LargestThreeNumbers.png)

*Screenshot of Skillset 9 - Array Runtime Data Validation*:

![ArrayRuntime](imgP/ArrayRuntime.png)


#### Tutorial Links:

* Tutorial - Helper_videos: *

[A2 Healthy Recipe Tutorial Link](http://www.qcitr.com/vids/LIS4381A2.mp4/ "Helper_Video")

[A3 Mobile App Requirements Tutorial Link](http://www.qcitr.com/vids/LIS4381_A3.mp4)
