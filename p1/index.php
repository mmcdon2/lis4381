<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Michael McDonald II">
    <link rel="icon" href="imgP/favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 1. Create a mobile business card app using Android Studio
2. Must change the background in both activities, add border to app image, and add text shadow to app button
3. Complete skillsets 7 through 9
4. Provide screenshots of both interfaces
5. Complete Project 1 Questions 
				</p>

				<h4>Business Card Interface 1</h4>
				<img src="imgP/business_card1.png" class="img-responsive center-block" alt="Business Card 1">

				<h4>Business Card Interface 2</h4>
				<img src="imgP/business_card2.png" class="img-responsive center-block" alt="Business Card 2">

				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
