# LIS4381 - Mobile Web Applications Development

## Michael McDonald

### Project 2 Requirements:

*Five Parts*

1. Clone A5 files
2. Copy A5 add_petstore.php and add_petstore_process.php files and rename it add_petstore.php
3. Open edit_petstore.php and edit_petstore_process.php files and modify meta tags and code
4. Create delete_petstore.php file
5. Create test and rss directory
6. Create index.php for RSS and choose RSS feed to input 
7. Helper_video links for this assignment


#### README.md file should include the following items:

* Project 2 requirements
* Screenshot of carousel
* Screenshot of index.php
* Screenshot of edit_petstore.php
* Screenshot of failed and validation
* Screenshot of delete record prompt and successfully deleted record
* Screenshot of RSS feed 
* Link to local lis4381 web app


#### Assignment Screenshots:

*Screenshot of carousel*:

![Carousel](img/carousel.png)

*Screenshot of index.php*:

![Index.php](img/index.php.png)

*Screenshot of edit_petstore.php*:

![Edit_petstore.php](img/edit_petstore.php.png)

*Screenshot failed validation*:

![Failed validation](img/failed_validation.png)

*Screenshot of passed validation*:

![Passed validation](img/passed_validation.png)

*Screenshot of delete record prompt*:

![Delete Record Prompt](img/delete_record_prompt.png)

*Screenshot of sucessfully deleted record*:

![Sucessfully Deleted Record](img/successfully_deleted_record.png)

*Screenshot of RSS feed*:

![RSS Feed](img/rss_feed.png)

#### Tutorial Links:

* Tutorial - Helper_videos:*

[P2 Requirements Tutorial 1 Link](http://www.qcitr.com/vids/LIS4381_P2_a.mp4)

[P2 Requirements Tutorial 2 Link](http://www.qcitr.com/vids/LIS4381_P2_b.mp4)

[P2 Requirements Tutorial 3 Link](http://www.qcitr.com/vids/LIS4381_P2_RSS_Feed.mp4)

*Bitbucket Repo Link:*

[Bitbucket LIS4381 Link](https://bitbucket.org/mmcdon2/lis4381/src/master/)

